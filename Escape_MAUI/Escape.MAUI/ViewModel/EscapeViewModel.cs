﻿using Escape.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Drawing;
using Escape.MAUI.View;

namespace Escape.MAUI.ViewModel
{
    public class EscapeViewModel : ViewModelBase
    {
        #region Fields
        //using a model instance to exchange information
        private EscapeGameModel _model;

        #endregion

        #region Properties
        // commands of the menu
        public DelegateCommand NewGameCommand { get; private set; }

        public DelegateCommand ChangeTableSizeCommand { get; private set; }
        
        public DelegateCommand LoadGameCommand { get; private set; }

        public DelegateCommand SaveGameCommand { get; private set; }

        public DelegateCommand ExitGameCommand { get; private set; }

        public DelegateCommand PauseGameCommand { get; private set; }

        public DelegateCommand SettingsCommand { get; private set; }

        public DelegateCommand BackCommand { get; private set; }

        public ObservableCollection<EscapeField> Fields { get; set; } //the list which contains all fields
        public string PauseText { get; private set; }
        public string SettingsButtonText { get; }

        //stepping commands (arrow keys on the keyboard) which tell the model to step the player
        public DelegateCommand StepUp { get; private set; }
        public DelegateCommand StepDown { get; private set; }
        public DelegateCommand StepLeft { get; private set; }
        public DelegateCommand StepRight { get; private set; }

        public int TableSize { get {  return _model.Table.Size; } }
        
        public String GameTime { get { return TimeSpan.FromSeconds(_model.GameTime).ToString("g"); } }

        public RowDefinitionCollection GameTableRows
        {
            get => new RowDefinitionCollection(Enumerable.Repeat(new RowDefinition(GridLength.Star), TableSize).ToArray());
            
        }

        public ColumnDefinitionCollection GameTableColumns
        {
            get => new ColumnDefinitionCollection(Enumerable.Repeat(new ColumnDefinition(GridLength.Star), TableSize).ToArray());
        }
        #endregion

        #region Event Handlers

        public event EventHandler NewGame;
        public event EventHandler ChangeTableSize;
        public event EventHandler LoadGame;
        public event EventHandler SaveGame;
        public event EventHandler ExitGame;
        public event EventHandler OpenSettings;
        public event EventHandler BackButtonPressing;

        #endregion

        #region Constructor

        public EscapeViewModel(EscapeGameModel model)
        {
            
            _model = model;

            //pairing the commands to eventhandlers
            _model.Stepping += new EventHandler<EscapeStepEventArgs>(Model_Stepping);
            _model.GameAdvanced += new EventHandler<EscapeEventArgs>(Model_GameAdvanced);
            _model.GameCreated += new EventHandler<EscapeEventArgs>(Model_GameCreated);
            _model.GameOver += new EventHandler<EscapeEventArgs>(Model_OnGameOver);

            NewGameCommand = new DelegateCommand(param => OnNewGame());
            ChangeTableSizeCommand = new DelegateCommand(param => OnChangeTableSize());
            LoadGameCommand = new DelegateCommand(param => OnLoadGame());
            SaveGameCommand = new DelegateCommand(param => OnSaveGame());
            ExitGameCommand = new DelegateCommand(param => OnExitGame());
            PauseGameCommand = new DelegateCommand(param => OnPauseGame());
            SettingsCommand = new DelegateCommand(param => OnOpenSettings());
            BackCommand = new DelegateCommand(param => OnBackButton());

            StepUp = new DelegateCommand(param => OnStepUp());
            StepDown = new DelegateCommand(param => OnStepDown());
            StepLeft = new DelegateCommand(param => OnStepLeft());
            StepRight = new DelegateCommand(param => OnStepRight());

            PauseText = "Start Game";
            SettingsButtonText = "Settings";

            //adding the fields into the list
            FieldAdding();
        }

        #endregion

        #region Private methods

        private void FieldAdding()
        {
            Fields = new ObservableCollection<EscapeField>();
            for (int i = 0; i < TableSize; i++)
            {
                for (int j = 0; j < TableSize; j++)
                {
                    Fields.Add(new EscapeField
                    {
                        IsMine = false,
                        IsPlayer = false,
                        IsEnemy = false,
                        IsEnemyDead = false,
                        Text = String.Empty,
                        X = i,
                        Y = j
                    });
                }
            }

            RefreshTable();
        }

        private void RefreshTable() //checks the field traits at each field
        {
            foreach (EscapeField field in Fields)
            {
                field.IsEnemy = _model.Table.IsEnemy(field.X, field.Y);
                field.IsPlayer = _model.Table.IsPlayer(field.X, field.Y);
                field.EnemyCaughtPlayer = _model.Table.GetValue(field.X, field.Y) == 3;
                field.IsMine = _model.Table.IsMine(field.X, field.Y);
                field.IsEnemyDead = _model.Table.IsEnemy(field.X, field.Y) && _model.Table.IsAnEnemyInAMine(field.X, field.Y);
                field.Text = _model.Table.IsMine(field.X, field.Y) ? "X" : String.Empty;
            }

            OnPropertyChanged();
        }
        public void OnPauseGame()
        {            
            if (!_model.IsGameOver)
            {
                if (_model.IsTimerEnabled())
                {
                    PauseText = "Continue";
                    _model.StopTimer();
                }

                else
                {
                    PauseText = "Pause Game";
                    _model.StartTimer();
                }
            }
            
            OnPropertyChanged(nameof(PauseText));
        }

        private void Model_OnGameOver(object sender, EscapeEventArgs e)
        {
            _model.StopTimer();
            PauseText = "Game is Over";

            OnPropertyChanged(nameof(PauseText));
        }

        private void OnOpenSettings()
        {
            if (_model.IsTimerEnabled())
            {
                OnPauseGame();
            } 
            OpenSettings?.Invoke(this, EventArgs.Empty);
        }

        private void OnBackButton()
        {
            BackButtonPressing?.Invoke(this, EventArgs.Empty);
        }

        #endregion

        #region Game event handlers

        private void Model_Stepping(object sender, EscapeStepEventArgs e) //eventhandler of the steppings 
        {
            RefreshTable();
        }

        private void Model_GameAdvanced(object sender, EscapeEventArgs e) //eventhandler of the game advancing 
        {
            OnPropertyChanged(nameof(GameTime));
        }

        private void Model_GameCreated(object sender, EscapeEventArgs e) //eventhandler of the game creating
        {
            RefreshTable();
        }

        #endregion

        #region Controlling private methods

        //the command methods of the player controlling.
        //if the timer is not enabled, the game might be over or paused (player cannot move then)
        private void OnStepUp()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new System.Drawing.Point(-1, 0));
        }

        private void OnStepDown()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new System.Drawing.Point(1, 0));
        }

        private void OnStepLeft()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new System.Drawing.Point(0, -1));
        }

        private void OnStepRight()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new System.Drawing.Point(0, 1));
        }

        #endregion

        #region Event methods

        private void OnNewGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            //RefreshTable();
            NewGame?.Invoke(this, EventArgs.Empty);
            RefreshTable();

            PauseText = "Start Game";
            OnPropertyChanged(nameof(PauseText));
        }

        private void OnChangeTableSize()
        {
            ChangeTableSize?.Invoke(this, EventArgs.Empty);
        }

        private void OnLoadGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            LoadGame?.Invoke(this, EventArgs.Empty);

            if (_model.IsTimerEnabled())
            {
                PauseText = "Pause Game";
                OnPropertyChanged(nameof(PauseText));
            }
        }

        private void OnSaveGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            SaveGame?.Invoke(this, EventArgs.Empty);
        }

        private void OnExitGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            ExitGame?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
