﻿using Escape.MAUI.ViewModel;
using Escape.Model;
using Escape.Persistence;
using Escape.MAUI.View;

namespace Escape.MAUI;

public partial class AppShell : Shell
{
	private readonly IEscapeDataAccess _escapeDataAccess;
	private readonly EscapeGameModel _escapeGameModel;
	private readonly EscapeViewModel _escapeViewModel;

	private readonly IStore _store;
	private readonly StoredGameBrowserModel _storedGameBrowserModel;
	private readonly StoredGameBrowserViewModel _storedGameBrowserViewModel;

    public event EventHandler ChangingTableSize;

    public AppShell()
    {
        InitializeComponent();
    }

	public AppShell(IEscapeDataAccess escapeDataAccess, 
		EscapeGameModel escapeGameModel, 
		EscapeViewModel escapeViewModel,
		IStore escapeStore)
	{
		InitializeComponent();
		_escapeDataAccess = escapeDataAccess;
		_escapeGameModel = escapeGameModel;
		_escapeViewModel = escapeViewModel;
		_store = escapeStore;

        _escapeGameModel.GameOver += EscapeGameModel_GameOver;

        //instancing a new viewmodel, and subscribing to the command events

        _escapeViewModel.NewGame += EscapeViewModel_NewGame;
        _escapeViewModel.ChangeTableSize += EscapeViewModel_ChangeSize;
        _escapeViewModel.LoadGame += EscapeViewModel_LoadGame;
        _escapeViewModel.SaveGame += EscapeViewModel_SaveGame;
        _escapeViewModel.BackButtonPressing += EscapeViewModel_BackButtonPressing;
        _escapeViewModel.OpenSettings += EscapeViewModel_OpenSettings;

        _storedGameBrowserModel = new StoredGameBrowserModel(_store);
        _storedGameBrowserViewModel = new StoredGameBrowserViewModel(_storedGameBrowserModel);
        _storedGameBrowserViewModel.GameLoading += StoredGameBrowserViewModel_GameLoading;
        _storedGameBrowserViewModel.GameSaving += StoredGameBrowserViewModel_GameSaving;
        _storedGameBrowserViewModel.BackBackButtonPressing += EscapeViewModel_BackButtonPressing;

#pragma warning disable CS0612 
#pragma warning disable CS0618 
        Device.InvokeOnMainThreadAsync(async () =>
        {
            await Navigation.PushAsync(new GamePage
            {
                BindingContext = _escapeViewModel
            });
        });
    }
#pragma warning restore CS0618 
#pragma warning restore CS0612 
    private async void EscapeGameModel_GameOver(object sender, EscapeEventArgs e)
    {
        if (e.IsWon)
        {
#pragma warning disable CS0612 
#pragma warning disable CS0618 
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                await DisplayAlert("You Won!" + Environment.NewLine +
                    "Your playtime was " +
                    TimeSpan.FromSeconds(e.GameTime).ToString("g"),
                    "Escape Game",
                    "OK");
            });
#pragma warning restore CS0618 
#pragma warning restore CS0612 
        }
        else
        {
#pragma warning disable CS0612 
#pragma warning disable CS0618 
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                await DisplayAlert("You Lost!",
                    "Escape Game",
                    "OK");
            });
#pragma warning restore CS0612
#pragma warning restore CS0618
        }
    }


    #region ViewModel event handlers

    private void EscapeViewModel_NewGame(object sender, EventArgs e)
    {
        Navigation.PopAsync();
        _escapeGameModel?.NewGame();
    }

    private void EscapeViewModel_ChangeSize(object sender, EventArgs e)
    {
#pragma warning disable CS0612
#pragma warning disable CS0618
        Device.BeginInvokeOnMainThread(async () =>
        {
            if (await DisplayAlert("Are you sure you want to change table size?", "Unsaved data will be lost.", "YES", "NO"))
            {
                ChangingTableSize?.Invoke(this, EventArgs.Empty);
            }
        });
#pragma warning restore CS0612
#pragma warning restore CS0618
    }

    private async void EscapeViewModel_LoadGame(object sender, EventArgs e)
    {
        await _storedGameBrowserModel.UpdateAsync();
        await Navigation.PushAsync(new LoadGamePage
        {
            BindingContext = _storedGameBrowserViewModel
        });
    }

    private async void EscapeViewModel_SaveGame(object sender, EventArgs e)
    {
        await _storedGameBrowserModel.UpdateAsync();
        await Navigation.PushAsync(new SaveGamePage
        {
            BindingContext = _storedGameBrowserViewModel
        });
    }

    private async void EscapeViewModel_OpenSettings(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new SettingsPage
        {
            BindingContext = _escapeViewModel
        });
    }

    private void EscapeViewModel_BackButtonPressing(object sender, EventArgs e)
    {
        Navigation.PopAsync();
    }

    private async void StoredGameBrowserViewModel_GameLoading(object sender, StoredGameEventArgs e)
    {
        await Navigation.PopAsync();

        try
        {
            await _escapeGameModel.LoadGameAsync(e.Name);

            await Navigation.PopAsync();
            await DisplayAlert("Escape", "Loading Game was successful!", "OK");
        }
        catch (InvalidOperationException)
        {
            await DisplayAlert("Escape", "Loading Game was unsuccessful!\nNOTE: You should try to open the file with the appropriate table size.", "OK");
        }

        catch
        {
            await DisplayAlert("Escape", "Loading Game was unsuccessful!", "OK");
        }
    }

    private async void StoredGameBrowserViewModel_GameSaving(object sender, StoredGameEventArgs e)
    {
        await Navigation.PopAsync();

        try
        {
            await _escapeGameModel.SaveGameAsync(e.Name);
            await DisplayAlert("Escape", "Loading Game was successful!", "OK");
        }
        catch
        {
            await DisplayAlert("Escape", "Loading Game was unsuccessful!", "OK");
        }
    }

    #endregion
}
