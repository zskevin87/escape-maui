﻿using Escape.Persistence;
using Escape.Model;
using Escape.MAUI.ViewModel;
using Escape.MAUI.Persistence;
using Microsoft.Maui.Controls;

namespace Escape.MAUI;

public partial class App : Application, IDisposable
{
    private const string SuspendedGameSavePath = "SuspendedGame";

    private AppShell _appShell;
    private readonly IEscapeDataAccess _escapeDataAccess;
    private EscapeGameModel _escapeGameModel;
    private readonly IStore _escapeStore;
    private EscapeViewModel _escapeViewModel;
    private readonly WelcomeViewModel _welcomeViewModel;

    public App()
	{
		InitializeComponent();
        _welcomeViewModel = new WelcomeViewModel();

        _welcomeViewModel.modelSetSize11 += OnSetSize11;
        _welcomeViewModel.modelSetSize15 += OnSetSize15;
        _welcomeViewModel.modelSetSize21 += OnSetSize21;

        _escapeStore = new EscapeStore();
        _escapeDataAccess = new EscapeFileDataAccess(FileSystem.AppDataDirectory);

        _appShell = new AppShell()
        {
            BindingContext = _welcomeViewModel
        };
        MainPage = _appShell;
    }

    private void OnSetSize11(object sender, EventArgs e)
    {
        _escapeGameModel = new EscapeGameModel(TableSize.ELEVEN, _escapeDataAccess);
        _escapeGameModel.NewGame();
        CommonSettings();
    }

    private void OnSetSize15(object sender, EventArgs e)
    {
        _escapeGameModel = new EscapeGameModel(TableSize.FIFTEEN, _escapeDataAccess);
        _escapeGameModel.NewGame();
        CommonSettings();
    }
    private void OnSetSize21(object sender, EventArgs e)
    {
        _escapeGameModel = new EscapeGameModel(TableSize.TWENTYONE, _escapeDataAccess);
        _escapeGameModel.NewGame();
        CommonSettings();
    }
#pragma warning disable CS0612
#pragma warning disable CS0618 
    private void CommonSettings()
    {
        _escapeViewModel = new EscapeViewModel(_escapeGameModel);

        _appShell = new AppShell(_escapeDataAccess, _escapeGameModel, _escapeViewModel, _escapeStore)
        {
            BindingContext = _escapeViewModel
        };
        Device.BeginInvokeOnMainThread(() =>
        {
            MainPage = _appShell;
        });
        _appShell.ChangingTableSize += Changing_TableSize;
    }
#pragma warning restore CS0612 
#pragma warning restore CS0618 

    private void Changing_TableSize(object sender, EventArgs e)
    {
        _escapeGameModel = null!;
        _escapeViewModel = null!;
        _appShell = new AppShell()
        {
            BindingContext = _welcomeViewModel
        };
        MainPage = _appShell;
        _appShell.ChangingTableSize += Changing_TableSize;
    }

    
    protected override Window CreateWindow(IActivationState activationState)
    {
        Window window = base.CreateWindow(activationState);

        window.Created += (s, e) =>
        {
            
        };

        window.Activated += (s, e) =>
        {
            if (File.Exists(Path.Combine(FileSystem.AppDataDirectory, SuspendedGameSavePath)))
            {
                Task.Run(async () =>
                {
                    try
                    {
                        using (StreamReader reader = new StreamReader(SuspendedGameSavePath))
                        {
                            String line = await reader.ReadLineAsync() ?? String.Empty;
                            Int32 tableSize = Int32.Parse(line);

                            switch (tableSize)
                            {
                                case 11:
                                    OnSetSize11(this, EventArgs.Empty);
                                    break;
                                case 15:
                                    OnSetSize15(this, EventArgs.Empty);
                                    break;
                                case 21:
                                    OnSetSize21(this, EventArgs.Empty);
                                    break;
                            }
                            await _escapeGameModel.LoadGameAsync(SuspendedGameSavePath);
                        }
                    }
                    catch
                    {
                    }
                });
            }
        };

        window.Deactivated += (s, e) =>
        {
            Task.Run(async () =>
            {
                if (_escapeGameModel != null)
                {
                    try
                    {
                        if (_escapeGameModel.IsTimerEnabled())
                            _escapeViewModel?.OnPauseGame();

                        await _escapeGameModel.SaveGameAsync(SuspendedGameSavePath);
                    }
                    catch
                    {
                    }
                }
            });
        };

        return window;
    }

    #region IDisposable implementation

    private bool disposed = false;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposed)
        {
            if (disposing)
            {
                _escapeGameModel?.Dispose();
            }

            disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    #endregion
}
