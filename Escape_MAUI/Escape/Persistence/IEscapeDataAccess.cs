﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.Persistence
{
    public interface IEscapeDataAccess //the interface of the datamanaging function
    {
        Task<EscapeTable> LoadAsync(String path); //loading game

        Task SaveAsync(String path, EscapeTable table, int time); //saving game
    }
}
